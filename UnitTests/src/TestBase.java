import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.testng.annotations.*;
import static org.testng.Assert.*;

/**
 * Created by Rail on 11.04.2016.
 */
public class TestBase {
    protected AppManager app;
    protected StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        app = new AppManager();
        app.driver.manage().window().maximize();
        app.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        app.navigationHelper.goToHomePage();

    }

    @AfterClass(alwaysRun = true)
    protected void tearDown() throws Exception {
        app.Stop();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public String generateRandomString(int size){
        StringBuilder result = new StringBuilder();
        String symbols = "qwertyuioplkjhgfdsazxcvbnm";
        Random random = new Random();
        for (int i=0;i< size;i++){
            result.append(symbols.charAt(random.nextInt(symbols.length())));
        }
        return  result.toString();
    }
}
